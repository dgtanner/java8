package com.dgtanner.java8.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class FindFirst {

	public static void main(String[] args) {
		int x = 1;
		int y = 2;
		List<String> myStrings = (x == y ? Collections.emptyList() : new ArrayList<>());
		
		List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		
		Predicate<Integer> isEven = n -> n%2 == 0;
		Predicate<Integer> isGreaterThanTwo = n -> n > 2;
		
		printMatchingNumber(nums, isEven);
		printMatchingNumber(nums, isEven.negate());
		printMatchingNumber(nums, isEven.and(isGreaterThanTwo));
		
	}

	public static void printMatchingNumber(List<Integer> nums, Predicate<Integer> test) {
		Integer first = nums.stream()
				.filter(test)
				.findFirst()
				.orElse(0);
				
		System.out.println(first);
	}
	

}
