package com.dgtanner.java8.streams;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

public class LazyVsTerminal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doStuff();
	}
	
	public static void doStuff() {
		Map<String, Integer> numbersMap = new HashMap<>(); 
		numbersMap.put("one", 1);
		numbersMap.put("two", 2);
		numbersMap.put("three", 3);
		numbersMap.put("four", 4);
		numbersMap.put("five", 5);
		
		Function<String, Integer> getMyNumber = n -> {
			System.out.println("called getMyNumber");
			return numbersMap.get(n);
		};
		
		Predicate<Integer> greaterThanTwo = n -> {
				System.out.println("called greaterThanTwo");
				return n > 2;
		};
		
		Predicate<Integer> even = n -> {
			System.out.println("called even");
			return n % 2 == 0;
		};
		
		
		
		
		List<String> lookup = Arrays.asList("one", "two", "three", "four", "five");
		//Noce that this reads more like a story
		//Super easy to make paralell
		Integer result = lookup.stream() //Give me all the strings
					   .map(getMyNumber) //get the integer for the string
					   .filter(greaterThanTwo) //filter out things that are not greater than two
					   .filter(even) //filter out things that are not even
					   .findFirst() //find the first thing that is resulting.
					   .orElse(-1); //If you don't find anything, give me -1
		System.out.println(result);
		
						
	}

}
