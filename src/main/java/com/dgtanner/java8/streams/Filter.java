package com.dgtanner.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Filter {

	public static void main(String[] args) {
		List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		
		//Don't want to go this route
		//printEvenNumbers(nums)
		//printOddNumbers(nums)
		//printEvenNumbersGreaterThanTwo(nums)
		
		
		//Better
		printMatchingNumbers(nums, n -> n%2 == 0); //print even numbers
		printMatchingNumbers(nums, n -> n%2 != 0); //print odd numbers
		printMatchingNumbers(nums, n -> n%2 == 0 && n > 2); //even numbers > 2
				
		//Best, things are easily reusable
		Predicate<Integer> isEven = n -> n%2 == 0;
		Predicate<Integer> isGreaterThanTwo = n -> n > 2;
		
		printMatchingNumbers(nums, isEven);
		printMatchingNumbers(nums, isEven.negate());
		printMatchingNumbers(nums, isEven.and(isGreaterThanTwo));
		
	}

	public static void printMatchingNumbers(List<Integer> nums, Predicate<Integer> test) {
		List<Integer> matchingNumbers = nums.stream()
				.filter(test) 
				.collect(Collectors.<Integer>toList());
		System.out.println(matchingNumbers);
	}
	
	
	
}
