package com.dgtanner.java8.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPI {

	//Lambda Bytecode is improved in java 8,(use invokedynamic)
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 int highestWeight = people.stream()
                 .filter(p -> p.getGender() == MALE)
                 .mapToInt(p -> p.getWeight())
                 .max();
		 
		 
		 
		 
		 int highestWeight = people.parallelStream()
                 .filter(p -> p.getGender() == MALE)
                 .mapToInt(p -> p.getWeight())
                 .max();
                 
                 */
		//Warning about making streams with primitives
		int[] nums = { 1, 2, 3, 4 };
		Stream.of(nums); // 1 item stream
		Arrays.stream(nums); //IntStream
		//Max, sum, min, avg, count etc
		
		
		Integer[] nums2 = { 1, 2, 3, 4 };
		Stream<Integer> integerStream = Stream.of(nums2);
		
		List<Integer> myInts = integerStream.<Integer>map(i -> i * i)
											.collect(Collectors.<Integer>toList());
		
		List<String> strings = Arrays.asList(new String[] {"test", "some", "strings"});
		
		List<Integer> list = myInts.stream()
								   .parallel()
								   .filter(p -> p > 1)
								   .collect(Collectors.<Integer>toList());
		
	}

}
