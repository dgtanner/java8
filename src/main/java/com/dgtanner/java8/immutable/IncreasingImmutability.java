package com.dgtanner.java8.immutable;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class IncreasingImmutability {
  public static void main(String[] args) {
   
	  
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
	
	//Imperative code, has mutation
	//How do you make this parallel?  Very difficult.
	
	int total = 0;
	for (int n: numbers) {
		total += n*2;
	}
	System.out.println(total);
	
    
	System.out.println(
			//This is easy to make parallel
		      numbers.stream()
		      .mapToInt(number -> number * 2)
		      .reduce(0, Integer::sum));
		      
    
  }
}
