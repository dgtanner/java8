package com.dgtanner.java8.functionalinterface;

import java.util.function.Function;

public interface ComplexFunctionalInterface extends SimpleFunctionalInterface {

	default public void goToWork() {
		System.out.println("Get up... Drive to work.... Work.... Drive Home....");
	}
	
}
