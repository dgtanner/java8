package com.dgtanner.java8.functionalinterface;

public class SimpleTest {

	public static void completeWork(SimpleFunctionalInterface sfi) {
		sfi.takeAVacation();
	}
	
	public static void main(String[] args) {

		//The old way
		completeWork(new SimpleFunctionalInterface() {
			
			@Override
			public void takeAVacation() {
				System.out.println(" :(  I'm on vacation in an anonymous class.");
				
			}
		});
		
		
		//The new way (lambda is used for the single abstract method that will be consumed.
		completeWork(() -> System.out.println("ALRIGHT! A real vacation."));
		
	}
	
}
