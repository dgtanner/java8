package com.dgtanner.java8.functionalinterface;

import java.util.function.Function;

@FunctionalInterface
public interface SimpleFunctionalInterface {

	//Error with the following commented out
	public void takeAVacation();
	//Can only declare one abstract method (other than inherited from java.lang.object)
	
	
	//these are ok since they're inherited
	public String toString();
	public boolean equals(Object o);
	
	//Error with this not commented
	//public void secondAbstract();
}
