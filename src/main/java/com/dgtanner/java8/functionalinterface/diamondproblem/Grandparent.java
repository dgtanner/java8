package com.dgtanner.java8.functionalinterface.diamondproblem;

public interface Grandparent {

	public void whoIsBest();
	
}
