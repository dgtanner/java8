package com.dgtanner.java8.functionalinterface.diamondproblem;

public interface Dad extends Grandparent {

	public default void whoIsBest() {
		System.out.println("Dad is best");
	}
	
}
