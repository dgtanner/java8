package com.dgtanner.java8.functionalinterface.diamondproblem;

public interface Mom extends Grandparent{

	public default void whoIsBest() {
		System.out.println("Mom is best");
	}
}
