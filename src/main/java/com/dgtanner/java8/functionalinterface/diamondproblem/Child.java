package com.dgtanner.java8.functionalinterface.diamondproblem;

public class Child implements Mom, Dad {
	
	/*
	public void whoIsBest() {
		Dad.super.whoIsBest();
	}
	*/

	public static void main(String[] args) {
		Child c = new Child();
		c.whoIsBest();
	}
}
