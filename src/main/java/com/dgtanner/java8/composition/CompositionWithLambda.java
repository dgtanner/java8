package com.dgtanner.java8.composition;

import java.util.Arrays;
import java.util.List;

public class CompositionWithLambda {
  public static boolean isGreaterThan3(int number) {
    return number > 3;
  }
  
  public static boolean isEven(int number) {
    return number % 2 == 0;
  }
  
  public static int doubleIt(int number) {
    return number * 2;
  }
  
  //Lazy streams
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
    
    
    
    
    //double the first even number greater than 3 from the list
    System.out.println(
      numbers.stream()
      	     .filter(CompositionWithLambda::isGreaterThan3)
      	     .filter(CompositionWithLambda::isEven)
      	     .map(CompositionWithLambda::doubleIt)
      	     .findFirst()
      	     .orElse(0)//Nothing happens til here
    );
  }
}

