package com.dgtanner.java8.lambda;

import java.util.function.Function;
import java.util.function.Predicate;

public class PredicateExample {

	public static void main(String[] args) {
		doStuff();
	}
	
	public static void doStuff() {
		Function<String, String> toLowerCase = String::toLowerCase;
		Function<String, String> toLowerCase2 = s -> s.toLowerCase();
		
		Predicate<String> containsALowercaseVowel = s -> s.contains("a") || s.contains("e") || s.contains("i") || s.contains("o") || s.contains("u");
		Predicate<String> isDavid = s -> s.equals("david");
		Predicate<String> combinedTest = containsALowercaseVowel.and(isDavid);
		
		String me = "DAVID";
		
		System.out.println(
				combinedTest.test(toLowerCase.apply(me))
				);
	}
	
}
