package com.dgtanner.java8.lambda;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Predicate;

public class BoilerPlateCodeRemoval {
/*
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static void oldWay() {
	    Collection<Person> people = …;

	    Iterator<Person> ip = people.iterator();
	    while (ip.hasNext()) {
	        Person p = ip.next();
	        if (p.getAge() > 18) {
	            ip.remove();
	        }
	    }
	}
	
	public static void newerWay() {
		Collections.removeAll(people,
                new Predicate<Person>() {
                    public boolean test(Person p) {
                        return p.getAge() > 18;
                    }
                });
	}
	
	public static void lambdaWay() {
		Collections.removeAll(people, p -> p.getAge() > 18);
	}
	*/
}
