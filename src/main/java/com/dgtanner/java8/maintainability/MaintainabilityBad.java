package com.dgtanner.java8.maintainability;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class MaintainabilityBad {
  public static void main(String[] args) {
   
	  
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
	
	System.out.println(totalValues(numbers));
	System.out.println(totalEvenValues(numbers));
	System.out.println(totalOddValues(numbers));
	
	//now only total all even numbers... we cut and paste the method... twice
		      
    
  }
  
  
  public static int totalValues(Collection<Integer> col) {
	  int total = 0;
	  for (int n : col) {
		  total += n;
	  }
	  return total;
  }
  
  
  
  public static int totalEvenValues(Collection<Integer> col) {
	  int total = 0;
	  for (int n : col) {
		  if (n%2 == 0) {
			  total += n;
		  }
	  }
	  return total;
  }
  
  public static int totalOddValues(Collection<Integer> col) {
	  int total = 0;
	  for (int n : col) {
		  if (n%2 != 0) {
			  total += n;
		  }
	  }
	  return total;
  }
}
