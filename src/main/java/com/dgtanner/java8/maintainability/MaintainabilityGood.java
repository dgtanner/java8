package com.dgtanner.java8.maintainability;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;


public class MaintainabilityGood {
  public static void main(String[] args) {
   
	  
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
	
	//This is using the strategy pattern.. (best pattern ever)  pass an algorithm that you would like to be used
	System.out.println(totalValues(numbers, e -> true));
	System.out.println(totalValues(numbers, e -> e%2 == 0));
	System.out.println(totalValues(numbers, e -> e%2 != 0));
    
  }
//see the last part below  
  //then see predicateexample
  //then see lazyvsterminal
  public static int totalValues(List<Integer> col, Predicate<Integer> p) {
	  int total = 0;
	  for (int n : col) {
		  if (p.test(n)) {
			  total += n;
		  }
	  }
	  return total;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  /*
  public static int totalValues(List<Integer> col, Predicate<Integer> p) {
	  //Line up the .'s
	  return col.stream()
			    .filter(p)
			    .reduce(0, (total, currentElement) -> total + currentElement);
	  			//Keep lambda simple (one line is best)
  }
 */
}
