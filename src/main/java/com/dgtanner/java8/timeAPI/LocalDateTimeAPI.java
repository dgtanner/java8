package com.dgtanner.java8.timeAPI;
import static java.lang.System.out;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class LocalDateTimeAPI {

	//JSR 310
	public static void main(String[] args) {
		localTimeDemo();
		//localDateDemo();
		//localDateTimeDemo();
	}
	
	
	
	//Hour, Minute, Second, down to nanosecond
	public static void localTimeDemo() {
		//Does not store time zone information.
		LocalTime now = LocalTime.now();
		out.println("Now:" + now);
		
		LocalTime fifteenMinutesFromNow = now.plusMinutes(15);
		out.println("Fifteen minutes from now:" + fifteenMinutesFromNow);
		
		//Modifying methods return a copy of the original
		out.println("LocalTime is immutable, original variable is not modified:" + now);
	}
	
	//Contains date information without the time
	public static void localDateDemo() {
		LocalDate today = LocalDate.now();
		out.println("Today:" + today);
		
		LocalDate yesterday = today.minusDays(1);
		out.println("Yesterday:" + yesterday);
	}
	
	public static void localDateTimeDemo() {
		
		LocalDateTime  yesterdayAtElevenThirty = LocalDate.now().minusDays(1).atTime(11, 30);
		out.println("Yesterday at Eleven Thirty:" + yesterdayAtElevenThirty);
		
		LocalDateTime specificDateTime = LocalDate.of(2013, 11, 20).atTime(11, 30);
		out.println("Specific date:" + specificDateTime);
		
		LocalDateTime oneWeekAgo = LocalDateTime.now().minusWeeks(1);
		out.println("One Week Ago:" + oneWeekAgo);
		
		LocalDateTime oneMonthFromNow = LocalDateTime.now().plusMonths(1);
		out.println("One Month From Now:" + oneMonthFromNow);
		
		LocalDateTime startOfToday = LocalDate.now().atStartOfDay();
		out.println("Beginning of today:" + startOfToday);
	}
	
	
}
