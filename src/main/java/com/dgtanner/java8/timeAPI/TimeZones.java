package com.dgtanner.java8.timeAPI;

import static java.lang.System.out;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeZones {

	//JSR 310
	public static void main(String[] args) {
		flightTime();
	}
	
	public static void flightTime() {
	
		ZoneId PNT = ZoneId.of("America/Phoenix");
		ZoneId PST = ZoneId.of("America/Los_Angeles");
		
		//Thanksgiving flight 
		LocalDate date = LocalDate.of(2013,  Month.NOVEMBER, 28);
		
		//Hours in of are zero based (0-23)
		LocalTime takeoff = LocalTime.of(12, 30); //12:30pm
		
		LocalTime land = LocalTime.of(16, 20); //4:20pm
		
		Duration flightTime = Duration.between(
				ZonedDateTime.of(date, takeoff, PNT), 
				ZonedDateTime.of(date, land, PST));
		
		out.println("Flight Time:" + flightTime); //In ISO8601 duration format
		out.println("Flight Time Hours:" + flightTime.toHours());
	}
}
