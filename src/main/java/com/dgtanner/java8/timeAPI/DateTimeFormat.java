package com.dgtanner.java8.timeAPI;

import static java.lang.System.out;
import static java.time.format.DateTimeFormatter.ISO_DATE;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateTimeFormat {

	public static void main(String[] args) {
		format();		
	}
	
	public static void format() {
		//new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");      NO MORE OF THIS INSANITY!!!
		//extensible. Can define CRE_DATE_FORMAT etc.
		
		out.println("Date Format:" + ISO_DATE.format(LocalDateTime.now()));
		
		
		out.println("Date Time Format:" + ISO_DATE_TIME.format(LocalDateTime.now()));
		
		//Have to be somewhat careful
		//ISO_DATE_TIME.format(LocalDate.now());
		
	}

}

/*
 http://docs.oracle.com/javase/tutorial/datetime/overview/naming.html
 http://docs.oracle.com/javase/tutorial/datetime/iso/overview.html
 * */
