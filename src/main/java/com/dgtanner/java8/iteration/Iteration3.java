package com.dgtanner.java8.iteration;

import java.util.Arrays;
import java.util.List;


public class Iteration3 {
	public static void main(String[] args) {
		currentPattern();
	}
	
	public static void currentPattern() {
		List<Integer> myIntegers = Arrays.asList(0,1,2,3,4,5,6,7,8,9);
		//This is still an external iterator
		//You manage all of this
		for (Integer integer : myIntegers) {
			System.out.println(integer);
		}
	}
}
