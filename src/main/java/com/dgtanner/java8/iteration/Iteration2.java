package com.dgtanner.java8.iteration;


public class Iteration2 {
	public static void main(String[] args) {
		arrays();
	}
	
	public static void arrays() {
		//This could be an array or a list.
		Integer[] myInts = {0,1,2,3,4,5,6,7,8,9};
		
		//this part is hard to do (familiar, but still very complex)
		
		//Must do four hard things
		//Set up variable
		//Set up boundary condition
		//Set up increment
		for (int x=0; x<myInts.length; x++) {
			//Must access by correct index
			System.out.println(myInts[x]);
		}
	}
}
