package com.dgtanner.java8.iteration;

import java.util.Arrays;
import java.util.List;


public class Iteration5 {
	public static void main(String[] args) {
		internatlIteration();
	}
	
	public static void internatlIteration() {
		List<Integer> myIntegers = Arrays.asList(0,1,2,3,4,5,6,7,8,9);

		//This removes all the boilerplate code and gives you the exact same thing
		myIntegers.forEach((Integer value) -> System.out.println(value));
		//can be further reduced to
		//The compiler knows this is an integer, you don't need to specify it
		myIntegers.forEach(value -> System.out.println(value));
		//can be further reduced to
		//Method references can only be used when you don't need to do anything intelligent.  (just passing though)
		myIntegers.forEach(System.out::println);
		
		/*
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		myIntegers.forEach(System.out::println);
		*/
		
		
		// This is not simply creating a new anonymous inner class.
	}
}
