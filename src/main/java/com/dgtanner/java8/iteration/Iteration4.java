package com.dgtanner.java8.iteration;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;


public class Iteration4 {
	public static void main(String[] args) {
		consumerPatternInternalIteration();
	}
	
	public static void consumerPatternInternalIteration() {
		List<Integer> myIntegers = Arrays.asList(0,1,2,3,4,5,6,7,8,9);
		
		//Hey collection, I don't care about how we do the loop, do it for me.
		myIntegers.forEach(new Consumer<Integer>() {
			//Consumer is a new interface in java 8
			//This is an internal iterator
			public void accept(Integer number) {
				System.out.println(number);
			}
		});
	}
}
