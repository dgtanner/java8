package com.dgtanner.java8.iteration;

import java.util.Enumeration;
import java.util.Vector;

public class Iteration1 {
	public static void main(String[] args) {
		dinosaurPattern();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void dinosaurPattern() {
	    Vector myIntegers = new Vector();
	    myIntegers.add(0);
	    myIntegers.add(1);
	    myIntegers.add(2);
	    myIntegers.add(3);
	    myIntegers.add(4);
	    myIntegers.add(5);
	    myIntegers.add(6);
	    myIntegers.add(7);
	    myIntegers.add(8);
	    myIntegers.add(9);
	    
	    Enumeration enumIntegers = myIntegers.elements();
	    
	    while (enumIntegers.hasMoreElements()){
	       System.out.println(enumIntegers.nextElement()); 
	    }
		
	}
}
