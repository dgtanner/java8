package com.dgtanner.java8.maps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordCount {

	private static String phrase = "the quick brown fox jumps over the lazy dog";
	
	public static void main(String[] args) {
		
		List<String> words = Arrays.asList(phrase.split(" "));
		Map<String, Integer> wordCount = new HashMap<>();
		
		
		
		for (String word: words) {
			Integer count = wordCount.get(word);
			if (count == null) {
				wordCount.put(word, 1);
			} else {
				wordCount.put(word, ++count);
			}
		}
		
		
		for (String word: words) {
			wordCount.compute(word,(k,v) -> v==null ? 1 : ++v);
		}
		
		
		words.forEach(word -> wordCount
							.compute(word, (k,v) -> v==null ? 1 : ++v));
		
		//words.forEach(System.out::println);
		
		//words.stream().forEach(System.out::println);
		
		words.stream()
			 .parallel()
			 .forEach(s -> System.out.println(s));
		
		
		for (Entry<String, Integer> entry: wordCount.entrySet()) {
			//out.println("Word:" + entry.getKey() + " Count:" + entry.getValue());
		}
		
		//Slide 53

	}

}
