package com.dgtanner.java8.maps;

import static java.lang.System.out;
import java.util.HashMap;
import java.util.Map;

public class NewFibonacci {

	private static Map<Integer,Long> memo = new HashMap<>();
	static {
	   memo.put(0,0L); //fibonacci(0)
	   memo.put(1,1L); //fibonacci(1)
	}
	
	public static long getFibonacciNumber(int x) {
		   return memo.computeIfAbsent(x, n -> getFibonacciNumber(n-1) + getFibonacciNumber(n-2));
	}
	
	
	public static void main(String[] args) {
		for (int n=0; n<93; n++) {
			out.println("N:" + n + " " + getFibonacciNumber(n));
		}
	}
	
}
