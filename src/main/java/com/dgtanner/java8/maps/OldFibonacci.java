package com.dgtanner.java8.maps;

import static java.lang.System.out;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class OldFibonacci {

	private static Map<Integer,Long> memo = new HashMap<>();
	static {
	   memo.put(0,0L); //fibonacci(0)
	   memo.put(1,1L); //fibonacci(1)
	}
	
	public static long getFibonacciNumber(int x) {
		
		return memo.computeIfAbsent(x, new Function<Integer, Long>() {
			@Override
			public Long apply(Integer key) {
				return (getFibonacciNumber(key));
			}
		});
		
	}
	
	
	public static void main(String[] args) {
		for (int n=0; n<93; n++) {
			out.println("N:" + n + " " + getFibonacciNumber(n));
		}
	}
	
}
