package java7review;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class java7 {

	private int tenMillion = 10000000;
	// Can now use underscores
	private int newTenMillion = 10_000_000;

	
	
	
	
	
	private List<String> listOfString = new ArrayList<String>();
	//Can now use the diamond operator
	private List<String> newListOfString = new ArrayList<>();

	
	
	
	
	
	
	
	static String readFirstLineFromFileWithFinallyBlock(String path) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(path));
		try {
			return br.readLine();
		} finally {
			if (br != null) {
				br.close();
			}
		}
	}
	
	//Can now use the new "try-with-resource" statements
	//This will work with any class that implements "AutoCloseable"
	//This adds the potential to have "supressed" exceptions
	//See https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html#suppressed-exceptions
	static String readFirstLineFromFile(String path) throws IOException {
	    try (BufferedReader br = new BufferedReader(new FileReader(path))) { //exceptions thrown with these statements may be supressed
	        return br.readLine();
	    }
	}

}
